package by.ggs.financialplanning;

public interface TransactionListener {
    public void update();
}

package by.ggs.financialplanning;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import by.ggs.financialplanning.Activity.HomeActivity;

public class Start_with_slider extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_with_slider);
        Button button = (Button)findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent neww = new Intent(Start_with_slider.this, HomeActivity.class);
                startActivity(neww);
            }
        });
    }
}
package by.ggs.financialplanning.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import by.ggs.financialplanning.R;
import by.ggs.financialplanning.model.Transaction5;

public class RVAdapter_INCOMES extends RecyclerView.Adapter<RVAdapter_INCOMES.TransactionViewHolder>{
    private static final int NUM_OF_ITEMS_TO_SHOW = 10;
    private static List<Transaction5> transaction5s = new ArrayList<>();
    Context context;

    public interface OnItemLongClickListener {
        public boolean onItemLongClicked(int position);
    }

    public static class TransactionViewHolder extends RecyclerView.ViewHolder{
        CardView cv;
        TextView tvTransactionName;
        TextView tvTransactionDate;
        TextView tvTransactionAmount;
        View view;

        public TransactionViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.card_view);
            tvTransactionName = (TextView)itemView.findViewById(R.id.tvTransactionName);
            tvTransactionDate = (TextView)itemView.findViewById(R.id.tvTransactionDate);
            tvTransactionAmount = (TextView)itemView.findViewById(R.id.tvTransactionAmount);
            view = itemView;
        }
    }

    public RVAdapter_INCOMES(Context context){
        this.context = context;
        //update();
    }

    @Override
    public TransactionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_item, parent, false);
        TransactionViewHolder tvh = new TransactionViewHolder(v);
        return tvh;
    }

    @Override
    public void onBindViewHolder(TransactionViewHolder holder, final int position) {
        holder.tvTransactionName.setText(transaction5s.get(position).getName());
        holder.tvTransactionDate.setText(transaction5s.get(position).getDateString());

        int amount = transaction5s.get(position).getAmount();
        String formattedAmount = String.format("%,d", amount);
        holder.tvTransactionAmount.setText(formattedAmount);

        holder.view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Action")
                        .setItems(R.array.edit_or_delete, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(which == 0){
                                                   //Delete
                                    Transaction5 toBeDeleted = transaction5s.get(position);
                                    List<Transaction5> trs = Transaction5.listAll(Transaction5.class);
                                    for(int i = 0; i < trs.size(); ++i){
                                        Transaction5 temp = trs.get(i);
                                        if(temp.getName().equals(toBeDeleted.getName()) &&
                                                temp.getAmount() == toBeDeleted.getAmount() &&
                                                temp.getCategory() == toBeDeleted.getCategory() &&
                                                temp.getRecurr() == toBeDeleted.getRecurr() &&
                                                temp.getDate().compareTo(toBeDeleted.getDate()) == 0){
                                            temp.delete();
                                            break;
                                        }
                                    }

                                    ((RVAdapter_INCOMES.OnItemLongClickListener) context).onItemLongClicked(position);
                                }
                            }
                        });
                builder.create();
                builder.show();

                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return transaction5s.size();
    }

    private void sortTransactions(){
        transaction5s.clear();
        transaction5s = Transaction5.listAll(Transaction5.class);
        Comparator<Transaction5> comparator = new Comparator<Transaction5>() {
            @Override
            public int compare(Transaction5 o1, Transaction5 o2) {
                return o1.getDate().compareTo(o2.getDate());
            }
        };
        Collections.sort(transaction5s, comparator);
        Collections.reverse(transaction5s);
    }

    public List<Transaction5> update(){
        sortTransactions();
        List<Transaction5> temp = new ArrayList<Transaction5>();
        for(int i = 0; i < transaction5s.size(); ++i){
            if(transaction5s.get(i).getAmount() > 0)
                temp.add(transaction5s.get(i));
        }
        transaction5s.clear();
        transaction5s = temp;
        this.notifyDataSetChanged();

        return transaction5s;
    }

    public List<Transaction5> update(int year, int month){
        sortTransactions();
        List<Transaction5> temp = new ArrayList<>();
        for(int i = 0; i < transaction5s.size(); ++i){
            if(transaction5s.get(i).getYear() == year && transaction5s.get(i).getMonth() == month && transaction5s.get(i).getAmount() > 0){
                temp.add(transaction5s.get(i));
            } else if(transaction5s.get(i).getYear() < year)
                break;
        }
        transaction5s.clear();
        transaction5s = temp;
        this.notifyDataSetChanged();

        return transaction5s;
    }

    public void updateByRecurr(){
        sortTransactions();
        List<Transaction5> temp = new ArrayList<>();
        for(int i = 0; i < transaction5s.size(); ++i){
            if(transaction5s.get(i).getRecurr() && transaction5s.get(i).getAmount() > 0)
                temp.add(transaction5s.get(i));
        }
        transaction5s.clear();
        transaction5s = temp;
        this.notifyDataSetChanged();
    }

    public List<Transaction5> getSortedTransactions(){
        List<Transaction5> tr = Transaction5.listAll(Transaction5.class);
        Comparator<Transaction5> comparator = new Comparator<Transaction5>() {
            @Override
            public int compare(Transaction5 o1, Transaction5 o2) {
                return o1.getDate().compareTo(o2.getDate());
            }
        };
        Collections.sort(tr, comparator);
        Collections.reverse(tr);
        return tr;
    }
}

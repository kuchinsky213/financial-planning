package by.ggs.financialplanning.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.icu.util.Calendar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import by.ggs.financialplanning.R;
import by.ggs.financialplanning.model.Transaction5;

public class RVAdapter_HOME extends RecyclerView.Adapter<RVAdapter_HOME.TransactionViewHolder>{
    private static final int NUM_OF_ITEMS_TO_SHOW = 10;
    private static List<Transaction5> transaction5s;
    private Context context;

    //longclick listener for the recycler view items
    public interface OnItemLongClickListener {
        public boolean onItemLongClicked(int position);
    }

    public static class TransactionViewHolder extends RecyclerView.ViewHolder{
        CardView cv;
        TextView tvTransactionName;
        TextView tvTransactionDate;
        TextView tvTransactionAmount;
        View view;

        public TransactionViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.card_view);
            tvTransactionName = (TextView)itemView.findViewById(R.id.tvTransactionName);
            tvTransactionDate = (TextView)itemView.findViewById(R.id.tvTransactionDate);
            tvTransactionAmount = (TextView)itemView.findViewById(R.id.tvTransactionAmount);
            view = itemView;
        }
    }

    public RVAdapter_HOME(Context context){
        this.context = context;
        update();
    }

    @Override
    public TransactionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_item, parent, false);
        TransactionViewHolder tvh = new TransactionViewHolder(v);
        return tvh;
    }

    @Override
    public void onBindViewHolder(final TransactionViewHolder holder, final int position) {
        holder.tvTransactionName.setText(transaction5s.get(position).getName());
        holder.tvTransactionDate.setText(transaction5s.get(position).getDateString());

        //format the amount before setting it to the textview -> 100,000,000
        int amount = transaction5s.get(position).getAmount();
        String formattedAmount = String.format("%,d", amount);
        holder.tvTransactionAmount.setText(formattedAmount);

        //setting the color of the text view depending on the amount (expense or income)
        if(transaction5s.get(position).getAmount() > 0){
            holder.tvTransactionName.setTextColor(Color.rgb(14, 71, 43));
            holder.tvTransactionAmount.setTextColor(Color.rgb(79, 150, 95));
            holder.tvTransactionDate.setTextColor(Color.rgb(79, 150, 95));
        }
        else {
            holder.tvTransactionName.setTextColor(Color.rgb(249, 79, 79));
            holder.tvTransactionAmount.setTextColor(Color.rgb(249, 79, 79));
            holder.tvTransactionDate.setTextColor(Color.rgb(249, 79, 79));
        }

        holder.view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Action")
                        .setItems(R.array.edit_or_delete, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(which == 0){
                                                                       //Delete the selected transaction
                                    Transaction5 toBeDeleted = transaction5s.get(position);
                                    List<Transaction5> trs = Transaction5.listAll(Transaction5.class);
                                    for(int i = 0; i < trs.size(); ++i){
                                        Transaction5 temp = trs.get(i);
                                        if(temp.getName().equals(toBeDeleted.getName()) &&
                                                temp.getAmount() == toBeDeleted.getAmount() &&
                                                temp.getCategory() == toBeDeleted.getCategory() &&
                                                temp.getRecurr() == toBeDeleted.getRecurr() &&
                                                temp.getDate().compareTo(toBeDeleted.getDate()) == 0){
                                            temp.delete();
                                            break;
                                        }
                                    }

                                    //call the activitiy's onItemLongClicked function to update the view
                                    ((OnItemLongClickListener) context).onItemLongClicked(position);
                                }
                            }
                        });
                builder.create();
                builder.show();

                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return transaction5s.size();
    }

    public List<Transaction5> update(){
        transaction5s = Transaction5.listAll(Transaction5.class);
        Comparator<Transaction5> comparator = new Comparator<Transaction5>() {
            @Override
            public int compare(Transaction5 o1, Transaction5 o2) {
                return o1.getDate().compareTo(o2.getDate());
            }
        };
        Collections.sort(transaction5s, comparator);
        Collections.reverse(transaction5s);
        List<Transaction5> temp = new ArrayList<Transaction5>();
        int items_to_show = transaction5s.size() > NUM_OF_ITEMS_TO_SHOW ? NUM_OF_ITEMS_TO_SHOW : transaction5s.size();
        for(int i = 0; i < items_to_show; ++i){
            temp.add(transaction5s.get(i));
        }
        transaction5s = temp;
        this.notifyDataSetChanged();

        return transaction5s;
    }

    public List<Transaction5> getSortedTransactions(){
        List<Transaction5> tr = Transaction5.listAll(Transaction5.class);
        Comparator<Transaction5> comparator = new Comparator<Transaction5>() {
            @Override
            public int compare(Transaction5 o1, Transaction5 o2) {
                return o1.getDate().compareTo(o2.getDate());
            }
        };
        Collections.sort(tr, comparator);
        Collections.reverse(tr);
        return tr;
    }

    //Ismétlődő tranzakciók felvétele automatikusan
    public void addRecurringTransactions(Date currDate){
        Calendar currC = Calendar.getInstance();
        currC.setTime(currDate);
        List<Transaction5> l = Select.from(Transaction5.class).where(Condition.prop("recurr").eq(1)).list();
        for(int i = 0; i < l.size(); ++i){
            Transaction5 old = l.get(i);
            Calendar oldC = Calendar.getInstance();
            oldC.setTime(old.getDate());
            old.delete();
            Transaction5 tr = old;
            while (oldC.compareTo(currC) <= 0) {
                tr = new Transaction5(old.getName(), old.getAmount(), oldC.getTime(), false, old.getCategory());
                tr.save();
                oldC.add(Calendar.MONTH, 1);
            }
            tr.setRecurr(true);
            tr.save();
        }
    }
}